﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordCounter;
using System;
using System.Collections.Generic;
using System.Text;

namespace WordCounter.Tests
{
    [TestClass()]
    public class ParaProcessorTests
    {
        [TestMethod()]
        public void CountWordsTest_One()
        {
            ParaProcessor paraProcessor = new ParaProcessor();
            paraProcessor.CountWords("one two three.\ntwo three.\nthree.");

            Assert.AreEqual(paraProcessor.wordCounts["one"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["two"], 2);
            Assert.AreEqual(paraProcessor.wordCounts["three"], 3);
        }

        [TestMethod()]
        public void CountWordsTest_Two()
        {
            ParaProcessor paraProcessor = new ParaProcessor();
            paraProcessor.CountWords("The dog eats food.\nThe cat eats food.\nThe man eats the food.");

            Assert.AreEqual(paraProcessor.wordCounts["the"], 4);
            Assert.AreEqual(paraProcessor.wordCounts["eats"], 3);
            Assert.AreEqual(paraProcessor.wordCounts["food"], 3);
            Assert.AreEqual(paraProcessor.wordCounts["dog"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["cat"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["man"], 1);
        }

        [TestMethod()]
        public void CountWordsTest_Three()
        {
            ParaProcessor paraProcessor = new ParaProcessor();
            paraProcessor.CountWords("Steve saw Chris' food.  Chris saw\nSteve's food.  Should they\ntrade food?");

            Assert.AreEqual(paraProcessor.wordCounts["saw"], 2);
            Assert.AreEqual(paraProcessor.wordCounts["chris"], 2); // punct at end removed.
            Assert.AreEqual(paraProcessor.wordCounts["food"], 3);
            Assert.AreEqual(paraProcessor.wordCounts["steve"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["steve's"], 1); // punct not at the end isn't removed.
            Assert.AreEqual(paraProcessor.wordCounts["they"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["trade"], 1);
        }

        [TestMethod()]
        public void CountWordsTest_Four()
        {
            ParaProcessor paraProcessor = new ParaProcessor();
            paraProcessor.CountWords("Double-Quote: \"I saw it.\"\nSingle-quote: 'I saw it.'\nParenthesis: (I saw it).");

            Assert.AreEqual(paraProcessor.wordCounts["double-quote"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["single-quote"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["parenthesis"], 1);
            Assert.AreEqual(paraProcessor.wordCounts["i"], 3);
            Assert.AreEqual(paraProcessor.wordCounts["saw"], 3);
            Assert.AreEqual(paraProcessor.wordCounts["it"], 3);
        }
    }
}