# WordCounter

## Build, Run and Test.

I built this as a .Net core console program with a single
class(ParaProcessor.cs) for processing the paragraph text and a single unit
test class(ParaProcessorTest.cs).  There are four unit tests.  I used the
community version of visual studio 2019 and .Net Core 3.1.  This is all on
windows 10.

If no command line parameter is provided, the console program opens a local
file (paragraph.in) and processes it and outputs to stdout.  If a parameter is
provided, and it is a file, it will process the file instead.

One way to build and test is to open the solution file
`WordCounter\WordCounter.sln` and use visual studio to build, run and test.

Alternatively, from the console, the following commands will pull, build and
run everything:

```
git clone https://gitlab.com/slambertwork/wordcounter.git
cd wordcounter\WordCounter
dotnet build
dotnet test
dotnet run
bin\Debug\netcoreapp3.1\WordCounter.exe temp.in
```

## Notes
* The paragraph.in file was sourced from
  [Wikipedia](https://simple.wikipedia.org/wiki/Paragraph)

## Issues

* The specification of a word was that it begins and end's with an
  alpha-numeric.  It can contain other chars in the middle, but that was the
  word boundary I chose.  Obviously, the definition of a word could be more
  sophisticated and would need further revisions to `addWord` to handle the
  them.  One thing would be an allowable set of of leading/traling punctuation
  chars.  The third and fourth unit tests address this issue.
* To simplify output, I altered the case.  This could be reverted if desired.
* For demonstration purposes, the command line assumes that the input file is a
  single paragraph.  Logic can be added to identify paragraph breaks
  (whitespace lines, indentation, etc.)
