﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordCounter
{
    public class ParaProcessor
    {
        public Dictionary<string, int> wordCounts { get; private set; }

        public ParaProcessor()
        {
            wordCounts = new Dictionary<string, int>();
        }

        public void CountWords(string paragraph)
        {
            StringBuilder wordToSave = new StringBuilder();
            wordCounts.Clear();

            foreach (char singleChar in paragraph)
            {
                // Whitespace singnals end of word.
                if (Char.IsWhiteSpace(singleChar))
                {
                    addWord(wordToSave);

                    // don't save again.
                    wordToSave.Clear();
                }

                // accumulate word while in contiguous letters and numbers
                else
                {
                    wordToSave.Append(singleChar);
                }
            }

            // catch last built word.
            addWord(wordToSave);
        }

        private void addWord(StringBuilder wordToSave)
        {
            // we have accumulated some number of non-whitespace chars.
            if (wordToSave.Length > 0)
            {
                // use unique words, ignore capitalization.
                string saveString = wordToSave.ToString().ToLower();

                // build custom punct list.
                List<char> charsToTrimList = new List<char>();
                foreach ( char c in saveString )
                {
                    if ( Char.IsPunctuation(c) )
                    {
                        charsToTrimList.Add(c);
                    }
                }

                // strip leading/lagging punct.
                saveString = saveString.TrimStart(charsToTrimList.ToArray());
                saveString = saveString.TrimEnd(charsToTrimList.ToArray());

                // add word to dictionary.
                if (wordCounts.ContainsKey(saveString))
                {
                    wordCounts[saveString]++;
                }

                else
                {
                    wordCounts[saveString] = 1;
                }
            }
        }
    }
}
