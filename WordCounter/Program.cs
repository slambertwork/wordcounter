﻿using System;
using System.Linq;
using System.IO;
using System.Text;

namespace WordCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            // delete this, but this needs to be where the solution run's from....
            string fileToRead = "paragraph.in";

            // Run provided file...
            if ( args.Length > 0 && File.Exists(args[0]) )
            {
                fileToRead = args[0];
            }

            string paragraph = File.ReadAllText(fileToRead, Encoding.UTF8);

            ParaProcessor paraProcessor = new ParaProcessor();
            paraProcessor.CountWords(paragraph);

            paraProcessor.wordCounts.ToList().ForEach(x => Console.WriteLine($"{x.Key}=>{x.Value}"));
        }
    }
}
